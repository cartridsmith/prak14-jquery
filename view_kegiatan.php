<?php
    include_once 'top.php';
    require_once 'db/class_kegiatan.php';
    //panggil file untuk operasi db
    //buat variabel utk menyimpan id
    //buat variabel untuk mengambil id
    $objKegiatan = new Kegiatan();
    $_id = $_GET['id'];
    $data = $objKegiatan->findByID($_id);
?>
<!--Buat tampilan dengan tabel-->

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">View Kegiatan</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered">
                <tr>
                <td class="active">Kode</td><td>:</td><td><?php echo
                $data['kode']?></td>
                </tr>
                <tr>
                <td class="active">Judul</td><td>:</td><td><?php echo
                $data['judul']?></td>
                </tr>
                <tr>
                <td class="active">Narasumber</td><td>:</td><td><?php
                echo $data['narasumber']?></td>
                </tr>
                <tr>
                <td class="active">Deskripsi</td><td>:</td><td><?php echo
                $data['deskripsi']?></td>
                </tr>
                <tr>
                <td class="active">Kategori</td><td>:</td><td><?php echo
                $data['kategori_id']?></td>
                </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<?php
    include_once 'bottom.php';
?>
