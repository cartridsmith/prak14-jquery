$(function() {
	$("form[name='form_kegiatan']").validate({
		rules: {
			kode: {
				required:true,
				maxlength:10,
			},
			judul: "required",
			narasumber: "required",
			deskripsi: "required",
		},

		messages: {
			kode: {
				required: "Kode wajib diisi",
				maxlength: "maksimum 10 character!!"
			},
			judul: "Judul wajib diisi!",
			narasumber: "Narasumber wajib diisi!",
			deskripsi: "Deskripsi wajib diisi",
		},

		submitHandler: function(form){
			form.submit();
		}
	});
});
