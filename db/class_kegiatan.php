<?php
    /*
    mysql> desc kegiatan;
    +-------------+--------------+------+-----+---------+----------------+
    | Field       | Type         | Null | Key | Default | Extra          |
    +-------------+--------------+------+-----+---------+----------------+
    | id          | int(11)      | NO   | PRI | NULL    | auto_increment |
    | kode        | varchar(10)  | NO   | UNI | NULL    |                |
    | judul       | text         | YES  |     | NULL    |                |
    | deskripsi   | text         | YES  |     | NULL    |                |
    | narasumber  | varchar(100) | YES  |     | NULL    |                |
    | kategori_id | int(11)      | NO   | MUL | NULL    |                |
    | biaya       | double       | YES  |     | NULL    |                |
    | kapasitas   | int(11)      | YES  |     | NULL    |                |
    | tgl_mulai   | date         | YES  |     | NULL    |                |
    | tgl_akhir   | date         | YES  |     | NULL    |                |
    | tempat      | varchar(100) | YES  |     | NULL    |                |
    +-------------+--------------+------+-----+---------+----------------+
    11 rows in set (0,00 sec)

    */
    require_once "DAO.php";
    class Kegiatan extends DAO
    {
        public function __construct()
        {
            parent::__construct("kegiatan");
        }

        public function simpan($data){
            $sql = "INSERT INTO ".$this->tableName.
            " (id,kode,judul,narasumber,deskripsi,kategori_id) ".
            " VALUES (default,?,?,?,?,?)";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }

        public function ubah($data){
            $sql = "UPDATE ".$this->tableName.
            " SET kode=?,judul=?,narasumber=?,deskripsi=?,kategori_id=? ".
            " WHERE id=?";
            $ps = $this->koneksi->prepare($sql);
            $ps->execute($data);
            return $ps->rowCount();
        }
        
	public function getStatistik() {
		$sql = "select a.nama,count(b.id) as jumlah from kategori a LEFT JOIN kegiatan b on a.id=b.kategori_id group by a.nama";
		$ps = $this->koneksi->prepare($sql);
		$ps->execute();
		return $ps->fetchAll();
	}

    }
?>
